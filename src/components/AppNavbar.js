import { Fragment, useContext} from 'react';
import {Navbar, Nav, Container, Button, Form, FormControl } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import icon from './icon/navbaricon.png'
import cart from './svg/cart.svg'

export default function AppNavbar() {
	
	const { user } = useContext(UserContext);

	/*
		Syntax:
			localStorage.getItem(propertyName)
	*/
	//const [user, setUser] = useState(localStorage.getItem("email"));
	//console.log(user)

	return(
		<Navbar className = "mb-5" bg="light" expand="lg">
		  <Container fluid>
		  	<Navbar.Brand as={Link} to="/" ><img src={icon} alt=""/></Navbar.Brand>
		    <Navbar.Brand as={Link} to="/" > Rocky Reaches |</Navbar.Brand>
		    <Navbar.Text >
				  ..your source of soulful books
			</Navbar.Text>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">

		      <Nav className="ml-auto me-auto my-2 my-lg-0" style={{maxHeight: '100px' }} navbarScroll>
		       {/* <Nav.Link as={Link} to="/">Home</Nav.Link>*/}
		       		{ <Nav.Link as={Link} to="/items">Products</Nav.Link>}

		        {/*{console.log(user.isAdmin)}*/}
		        { (user.id !== null) ?
		        	<Fragment>
						<Nav.Link as={Link} to="/orderitems">Order History</Nav.Link>
					    <Nav.Link as={Link} to="/logout"> Logout </Nav.Link>
					</Fragment>        	
			        :
		        	<Fragment>
		        		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		        		<Nav.Link as={Link} to="/login">Login</Nav.Link>
		        	</Fragment>
		        }

		      </Nav>
		      <Form className="d-flex">
		        <FormControl
		            type="search"
		            placeholder="     What do you need?"
		            className="me-2"
		            aria-label="Search"
		        />
		        <Button variant="outline-success">Search</Button>
		      </Form>
		      <Nav> <img src={cart} alt=""/> </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}	