import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Items from './pages/Items';
import ItemView from './pages/ItemView'
import ErrorPage from './pages/ErrorPage';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import OrderItems from './pages/OrderItems';
import { UserProvider } from './UserContext';
import './App.css';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(() => {
    fetch("https://rocky-reaches-97141.herokuapp.com/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, [])

  return (
     <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/items" element={<Items />} />
            <Route path="/items/:itemId" element={<ItemView/>} />
            <Route path="/orderitems" element={<OrderItems />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>     
      </Router>
    </UserProvider>

  );
}

export default App;
